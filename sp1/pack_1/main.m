clc; clear all; close all; format short

gamma_min = 0.1;
gamma_max = 5;

am = 1;
bm = 0.5;
a = 2;
b1 = -0.4;
b2 = -1;

t0 = bm/b1;
s0 = (am-a)/b1;
chyba =[];

for i = gamma_min:0.1:gamma_max
    gamma = i;
    gamma
    sim('model');
    if(i == gamma_min)
        y_1 = y;
        e_1 = e;
        ym_1 = ym;
        s_1 = s;
        t_1 = t;
        u_1 = u;
        ur_1 = ur;
    end
     if(i == 2)
        y_2 = y;
        e_2 = e;
        ym_2 = ym;
        s_2 = s;
        t_2 = t;
        u_2 = u;
        ur_2 = ur;
     end    
     chyba = [chyba sum(abs(e.Data))];
end

y_3 = y;
e_3 = e;
ym_3 = ym;
s_3 = s;
t_3 = t;
u_3 = u;
ur_3 = ur;




figure
hold on;
grid on;
title('V�stupy syst�mu')
plot(y_1.Time, y_1.Data);
plot(y_2.Time, y_2.Data);
plot(y_3.Time, y_3.Data);
plot(ym_1.Time, ym_1.Data,'k');
legend('\gamma 0.1','\gamma 2.0', '\gamma 5.0', 'y_m');


figure
hold on;
grid on;
title('V�voj chyby')
plot(e_1.Time, e_1.Data);
plot(e_2.Time, e_2.Data);
plot(e_3.Time, e_3.Data);
legend('\gamma 0.1','\gamma 2.0', '\gamma 5.0');

figure
hold on;
grid on;
title('Referencn� sign�l a regul�tory')
plot(u_1.Time, u_1.Data);
plot(u_2.Time, u_2.Data);
plot(u_3.Time, u_3.Data);
plot(ur_1.Time, -ur_1.Data,'k');
legend('\gamma 0.1','\gamma 2.0', '\gamma 5.0', '-u_r');

figure
hold on;
grid on;
title('V�voj parametru regul�toru')
subplot(3,1,1)
title('\gamma 0.1')
hold on;
plot(s_1.Time, s_1.Data);
plot(t_1.Time, t_1.Data);
legend('s','t')
subplot(3,1,2)
title('\gamma 2.0')
hold on;
plot(s_2.Time, s_2.Data);
plot(t_2.Time, t_2.Data);
legend('s','t')
subplot(3,1,3)
title('\gamma 5.0')
hold on;
plot(s_3.Time, s_3.Data);
plot(t_3.Time, t_3.Data);
legend('s','t')


figure
hold on
grid on
title('V�voj velikosti chyby pro jednotliv� \gamma')
plot(gamma_min:0.1:gamma_max, chyba);



